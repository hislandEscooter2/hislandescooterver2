package com.hisland.backend.controller;

import com.hisland.backend.BackendApplication;
import com.hisland.backend.service.ProcessService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BackendApplication.class)
public class LightControllerTest {

    @Autowired
    private WebApplicationContext wac;
    @MockBean
    ProcessService processService;
    private MockMvc mockMvc;
    private static final String CONTENT_TYPE = "application/x-www-form-urlencoded";


    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

    }


    @Test
    public void successLightsCall() throws Exception {
        mockMvc.perform(post("/light")
                .contentType(CONTENT_TYPE)
                .content("0")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    public void fail1LightsCall() throws Exception {
        mockMvc.perform(post("/lights")
                .contentType(CONTENT_TYPE)
                .content("0")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    public void fail2LightsCall() throws Exception {
        mockMvc.perform(post("/lights")
                .contentType(CONTENT_TYPE)
                .content("3")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    public void fail3LightsCall() throws Exception {
        mockMvc.perform(post("/lights")
                .contentType(CONTENT_TYPE)
                .content("ddd")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

   // @Test
   // public void getLight() {
   // }
}
