package com.hisland.backend.communication.receiver;

import com.hisland.backend.communication.Notifier;
import com.hisland.backend.config.MQTTInitializer;
import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * takes care of receiving mqtt messages and triggering the notifier for the listeners
 */
@Component
public class MQTTReceiver implements Receiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(MQTTReceiver.class);

    private IMqttClient mqttClient;


    private final MQTTInitializer mqtTinitializer;

    private final Notifier notifier;


    private ArrayList<String> subscribers;

    /**
     * we subscribe to the relevant topics here
     * @param mqtTinitializer
     * @param notifier
     */
    @Autowired
    public MQTTReceiver(MQTTInitializer mqtTinitializer, Notifier notifier) {
        this.mqtTinitializer = mqtTinitializer;

        this.notifier = notifier;
        this.subscribers = new ArrayList<>();
        subscribers.add("light/bool");
        subscribers.add("gps/coordinates");
        subscribers.add("power/bool");
        subscribers.add("battery/json");
        subscribers.add("diagnostics/battery/string");

    }

    /**
     * we initialize the connection to the mqtt broker
     */
    public void initializeAndConnectMQTTClient() {

        mqttClient =  mqtTinitializer.init();

        LOGGER.debug("Receiver initializeAndConnectMQTTClient() isConnected: " + mqttClient.isConnected());

        try {
            for (String s : subscribers) {
                mqttClient.subscribe(s);
            }

        } catch (MqttException e) {
            LOGGER.debug("MQTTClient subscribe() failed. Mqttclient connected: " + mqttClient.isConnected());
        }
    }

    /**
     * the method that triggers when a message is received
     */
    MqttCallback mqttCallback = new MqttCallback() {
        @Override
        public void connectionLost(Throwable cause) {
            LOGGER.debug("connectionLost to mqtt server");
        }

        @Override
        public void messageArrived(String topic, MqttMessage JSONMessageGPSData) {
            LOGGER.info(JSONMessageGPSData.toString() + "    getMQTTClientCallback()");
            notifier.notifyListeners(JSONMessageGPSData.toString(), topic);

        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken token) {

        }
    };


    public void getMQTTClientCallback() {
        mqttClient.setCallback(mqttCallback);

    }

    public IMqttClient getMqttClient() {
        return mqttClient;
    }

    public void reconnectClient() {
        mqttClient = mqtTinitializer.init();
        LOGGER.info("Error handled");
    }
}
