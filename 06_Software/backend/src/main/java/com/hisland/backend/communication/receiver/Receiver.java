package com.hisland.backend.communication.receiver;

import com.hisland.backend.exception.MyException;

public interface Receiver {
  void  initializeAndConnectMQTTClient() throws MyException;
  void  getMQTTClientCallback() throws MyException;
}
