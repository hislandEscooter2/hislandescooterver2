package com.hisland.backend.communication.processing;

import com.hisland.backend.exception.CommunicationException;
import com.hisland.backend.model.GPSCoordinate;

public interface Processor {
    void detect(String MQTTMessageData) throws CommunicationException;
}
