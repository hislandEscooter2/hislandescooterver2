package com.hisland.backend.model;

public class Fare {
    private String currency;
    private String value;
    private String text;


    // Getter Methods

    public String getCurrency() {
        return currency;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    // Setter Methods

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setText(String text) {
        this.text = text;
    }
}
