package com.hisland.backend.config;


import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfiguration {

    /**
     * modelmapper will transform JSON to POJO.
     * isn't being used now.
     * */
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
}

