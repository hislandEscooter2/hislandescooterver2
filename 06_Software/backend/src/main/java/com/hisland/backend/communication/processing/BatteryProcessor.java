package com.hisland.backend.communication.processing;

import com.hisland.backend.communication.Notifier;
import com.hisland.backend.communication.receiver.MQTTReceiver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * processes battery data
 */
@Component("battery/json")
public class BatteryProcessor extends Notifier implements Processor{
    private static final Logger LOGGER = LoggerFactory.getLogger(BatteryProcessor.class);

    private final
    SimpMessagingTemplate template;


    @Autowired
    public BatteryProcessor(SimpMessagingTemplate template) {
        this.template = template;
    }

    /**
     * sends MQTTMessageData to mqtt broker
     * @param MQTTMessageData
     */
    @Override
    public void detect(String MQTTMessageData) {
        LOGGER.info("BatteryProcessor IS ACTIVATED");
        template.convertAndSend("/batterystatus", MQTTMessageData);
    }
}
