package com.hisland.backend.repository;

import com.hisland.backend.model.ECU;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ECURepo extends JpaRepository<ECU, Integer> {
}
