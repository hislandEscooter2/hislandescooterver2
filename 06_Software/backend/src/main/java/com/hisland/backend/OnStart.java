package com.hisland.backend;
import com.hisland.backend.communication.receiver.Receiver;
import com.hisland.backend.exception.MyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class OnStart {
    private static final Logger LOGGER = LoggerFactory.getLogger(OnStart.class);
    @Value("${protocol.type}")
    private String protocolType;

    @Autowired
    private Receiver MQTTReceiver;

    void start() {
        LOGGER.debug("OnStart's start() called");
        if (protocolType.equals("mqtt")) {
            try {
                MQTTReceiver.initializeAndConnectMQTTClient();
                MQTTReceiver.getMQTTClientCallback();

            } catch (MyException e) {
                e.printStackTrace();
            }
        }
    }
}
