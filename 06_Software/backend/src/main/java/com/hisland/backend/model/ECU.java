package com.hisland.backend.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ECU {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Float batteryStatus;
    private Boolean powerOn;

    public ECU() {
    }

    public ECU(Float batteryStatus, Boolean powerOn) {
        this.batteryStatus = batteryStatus;
        this.powerOn = powerOn;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Float getBatteryStatus() {
        return batteryStatus;
    }

    public void setBatteryStatus(Float batteryStatus) {
        this.batteryStatus = batteryStatus;
    }

    public Boolean getPowerOn() {
        return powerOn;
    }

    public void setPowerOn(Boolean powerOn) {
        this.powerOn = powerOn;
    }
}
