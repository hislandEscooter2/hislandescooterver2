package com.hisland.backend.exception;

public class CommunicationException extends Exception {
    public CommunicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommunicationException(String message) {
    }
}
