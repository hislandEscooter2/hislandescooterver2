import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { DiagnosticsService } from './diagnostics.service';
import { Diagnostic } from './diagnostics.model';

@Component({
  selector: 'app-diagnostics',
  templateUrl: './diagnostics.page.html',
  styleUrls: ['./diagnostics.page.scss'],
})
export class DiagnosticsPage implements OnInit {
  public diagnosticDetails: Diagnostic[];
  private diagnosticDetail: Diagnostic;

  constructor(
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private diagnosticsService: DiagnosticsService) { }

  ngOnInit() {
    this.diagnosticDetails = this.diagnosticsService.getDiagnosticDetails()
  }

}
