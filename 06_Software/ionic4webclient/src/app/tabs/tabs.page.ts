import { Component, OnInit } from '@angular/core';
import { TabsService } from './tabs.service';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  constructor(private tabsService: TabsService) {
  }

  ionViewWillEnter(){
   
  }
  ngOnInit() {
    this.getVehicleStatus() 
  }
  
  async getVehicleStatus(){
    await this.tabsService.getVehiclesStatus()

  }



}
