import { Component, ViewChild, ElementRef, AfterViewInit, OnInit } from '@angular/core';
import { GoogleMap, Marker, LatLng } from '@ionic-native/google-maps';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { TabsService } from '../tabs.service';
import { Globals } from 'src/app/globals';

declare const google;

interface gpsDTOJSON {
    id: string;
    longitude: number;
    latitude: number;
}

@Component({
    selector: 'app-map',
    templateUrl: './map.page.html',
    styleUrls: ['./map.page.scss'],
})
export class MapPage implements AfterViewInit {

    batteryStatusView: string;
    identifier: string = "FHDK689";
    // google maps properties
    @ViewChild('map_canvas') mapElement: ElementRef;
    location = new google.maps.LatLng(57.710518, 11.959543);
    marker: any;
    map: GoogleMap;
    latitude: any;
    longitude: any;
    id: any = '123';

    // websockets properties
    private stompClient;

    constructor(
        private tabsService: TabsService,
        private globals: Globals) { }

    ngAfterViewInit() {
        this.startMap();
    }

    ionViewDidLeave() {
        const that = this;
        that.globals.stompClient.unsubscribe('/Gps');
        that.globals.stompClient.unsubscribe('/batterystatus');
    }

    ionViewDidEnter() {
        const _this = this;
        this.initializeWebSocketConnection();
        //this.batteryStatusView = this.tabsService.batteryStatus();
        
        //TODO: get id by rest call get
        // this.tabsService.identifier().subscribe(
        //     data => { _this.identifier = data });
        //this.identifier = this.tabsService.identifier();
    }

    //ws logic
    initializeWebSocketConnection() {
        const _this = this;
        this.globals.initializeWebSocketConnection(function () {
            _this.globals.stompClient.subscribe('/batterystatus', function (message) {
                console.log('message: ' + message.toString());
                console.log('body: ' + message.body);
                _this.batteryStatusView = message.body + "%"
            })
            _this.globals.stompClient.subscribe('/Gps', function (message) {
                console.log('message: ' + message.toString());
                console.log('body: ' + message.body);
                const obj: gpsDTOJSON = JSON.parse(message.body);
                _this.latitude = obj.latitude;
                _this.longitude = obj.longitude;
                _this.updateOnMap(obj.latitude, obj.longitude);
            });
           
        });
    }


    //google maps logic
    startMap(): any {
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
            zoom: 14,
            center: this.location,
            mapTypeId: 'roadmap'
        });
        this.marker = new google.maps.Marker({
            position: this.location,
            label: 'A',
            map: this.map
        });
        return this.map;
    }

    updateOnMap(lat: any, lng: any) {
        if (this.marker) {
            this.marker.setMap(null);
        }
        this.marker = this.addMarker(
            new LatLng(lat, lng),
            this.map,
            this.marker);
    }

    private addMarker(location, map, marker): any {
        marker = new google.maps.Marker({
            position: location,
            label: 'A',
            map
        });
        return marker;
    }
}
