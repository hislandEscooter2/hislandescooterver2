import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Globals } from '../globals';

@Injectable({
  providedIn: "root"
})
export class TabsService {
  private _light: boolean;
  private _power: boolean;
  private _batteryStatus: string;
  private _identifier: string;
  private httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };
  constructor(
    private http: HttpClient,
    private globals: Globals) { }

  async getVehiclesStatus() {
    this.callGetBatteryStatus();
    //fix identifier rest call (send ERROR)
    //this.callGetIdentifier();
    this.callGetLight();
    this.callGetPower();
  }

  callGetLight() {
    return this.http
      .get(this.globals.serverUrl + "/light", this.httpOptions)
      .subscribe(data => {
        if (data.toString() == "true") {
          this._light = true;
        } else {
          this._light = false;
        }
      });
  }

  callGetPower() {
    return this.http
      .get<String>(this.globals.serverUrl + "/power", this.httpOptions)
      .subscribe(data => {
        if (data.toString() == "true") {
          this._power = true;
        } else {
          this._power = false;
        }
      });
  }
  callGetBatteryStatus() {
    return this.http
      .get<String>(this.globals.serverUrl + "/batterystatus", this.httpOptions)
      .subscribe(data => {
        this._batteryStatus = data.toString();
      });
  }

  callGetIdentifier() {
    return this.http
      .get(this.globals.serverUrl + "/id", this.httpOptions)
      .subscribe(data => {
        this._identifier = data.toString();
      });
  }


  //getters and setters
  light(): boolean {
    return this._light;
  }

  power(): boolean {
    return this._power;
  }

  identifier(): string {
    return this._identifier;

  }

  batteryStatus(): string {
    return this._batteryStatus;
  }

  setLight(light: boolean) {
    this._light = light;
  }

  setPower(power: boolean) {
    this._power = power;
  }

  setBatteryStatus(batteryStatus: string) {
    this._batteryStatus = batteryStatus;
  }
}
