import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { TabsService } from './tabs.service';
var TabsPage = /** @class */ (function () {
    function TabsPage(tabsService) {
        this.tabsService = tabsService;
    }
    TabsPage.prototype.ionViewWillEnter = function () {
        console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
        this.tabsService.getVehiclesStatus();
        console.log("light: " + this.tabsService.light());
    };
    TabsPage.prototype.ngOnInit = function () {
    };
    TabsPage = tslib_1.__decorate([
        Component({
            selector: 'app-tabs',
            templateUrl: './tabs.page.html',
            styleUrls: ['./tabs.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [TabsService])
    ], TabsPage);
    return TabsPage;
}());
export { TabsPage };
//# sourceMappingURL=tabs.page.js.map