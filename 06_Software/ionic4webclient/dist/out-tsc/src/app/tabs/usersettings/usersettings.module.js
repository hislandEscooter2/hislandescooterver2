import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { UsersettingsPage } from './usersettings.page';
var routes = [
    {
        path: '',
        component: UsersettingsPage
    }
];
var UsersettingsPageModule = /** @class */ (function () {
    function UsersettingsPageModule() {
    }
    UsersettingsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [UsersettingsPage]
        })
    ], UsersettingsPageModule);
    return UsersettingsPageModule;
}());
export { UsersettingsPageModule };
//# sourceMappingURL=usersettings.module.js.map