import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { DiagnosticsPage } from './diagnostics.page';
var routes = [
    {
        path: '',
        component: DiagnosticsPage
    }
];
var DiagnosticsPageModule = /** @class */ (function () {
    function DiagnosticsPageModule() {
    }
    DiagnosticsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [DiagnosticsPage]
        })
    ], DiagnosticsPageModule);
    return DiagnosticsPageModule;
}());
export { DiagnosticsPageModule };
//# sourceMappingURL=diagnostics.module.js.map