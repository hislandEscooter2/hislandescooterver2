import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EvehiclelistPage } from './evehiclelist.page';

const routes: Routes = [
  {
    path: '',
    component: EvehiclelistPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EvehiclelistPage]
})
export class EvehiclelistPageModule {}
