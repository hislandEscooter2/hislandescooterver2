package com.hisland.simulator.model;

public class GPSCoordinate {
    private int id;
    private Double longitude;
    private Double latitude;

    public GPSCoordinate(int id, Double longitude, Double latitude) {
        this.id = id;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public GPSCoordinate() {
    }

    //correct json format {"id": 0, "longitude":57.710518, "latitude":11.948742999999999}
    @Override
    public String toString() {
        return "{" + "\"id\""+":" + id + ","+ "\"longitude\""+":" + latitude + "," +"\"latitude\"" + ":" + longitude + "}";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
