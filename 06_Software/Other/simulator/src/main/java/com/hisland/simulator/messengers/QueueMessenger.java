package com.hisland.simulator.messengers;


import com.hisland.simulator.converters.JSONConverter;
import com.hisland.simulator.model.GPSCoordinate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name = "messenger.type", havingValue = "queue")
public class QueueMessenger implements Messenger {
    private static final Logger LOGGER = LoggerFactory.getLogger(QueueMessenger.class);
    private final JSONConverter JSONConverter;
    private final RabbitTemplate rabbitTemplate;


    public QueueMessenger(JSONConverter JSONConverter, RabbitTemplate rabbitTemplate) {
        this.JSONConverter = JSONConverter;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void sendMessage(GPSCoordinate message) {
        String xmlString = message.toString();


        System.out.println(xmlString);
        rabbitTemplate.convertAndSend("springqueu", xmlString);
        LOGGER.info("QueueMessenger sent message");
        LOGGER.info("SENT" + message.toString());
    }
}
