/*
 Basic ESP8266 MQTT example
 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.
 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off
 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.
 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"
*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <SoftwareSerial.h>
#define LED 15
SoftwareSerial WemosSerial(0, 2);// RX TX


// Update these with values suitable for your network.

const char* ssid = "Hisland";//"Your WiFi SSID";
const char* password = "W#tofigo82fa1He";//"Your WiFi password";
const char* mqtt_server = "m24.cloudmqtt.com" ;//"Your MQTT Server IP address";
const char* mqtt_user = "rxhwkrwu";
const char* mqtt_password = "euP2FEqY5IlV";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
const byte numChars = 32;
char receivedChars[numChars];
boolean newData = false;
char coord[30];


void setup() {
  coord[0]= 'A';
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(LED, OUTPUT); 
//  Serial.begin(115200);
//  Serial.setDebugOutput(true);
  WemosSerial.begin(9600);
  Serial.print("I want coffee");
  digitalWrite(LED_BUILTIN,HIGH);
  digitalWrite(LED,LOW);
  setup_wifi();
  client.setServer(mqtt_server,14889);// server, Port
  client.setCallback(callback);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

//  // Switch on the LED if an 1 was received as first character
//  if ((char)payload[0] == '1') {
//    digitalWrite(LED_BUILTIN, LOW);   // Turn the LED on (Note that LOW is the voltage level
//    // but actually the LED is on; this is because
//    // it is acive low on the ESP-01)
//  } else {
//    digitalWrite(LED_BUILTIN, HIGH);  // Turn the LED off by making the voltage HIGH
//  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world init");
      // ... and resubscribe
      client.subscribe("inTopic");
    } if (client.connect("ESP8266Client")) {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  char rc;
  char endMarker = '\n';
  static byte ndx = 0;
  
  long now = millis();
  while(WemosSerial.available() > 0/* && newData == false*/) {
           //Serial.println(" try again in 5 seconds");
    lastMsg = now;
    ++value;
    rc = WemosSerial.read();
    if (rc != endMarker){
            //digitalWrite(LED_BUILTIN, HIGH);
            receivedChars[ndx] = rc;
            ndx++;
            if (ndx >= numChars) {
                ndx = numChars - 1;      
            }
      } else {
             //digitalWrite(LED, HIGH);
            delay(100);
            receivedChars[ndx] = '\0'; // terminate the string
            ndx = 0;
            newData = true;
            sscanf(receivedChars,"%s",&coord);
            //client.publish("outTopic",coord);
            client.publish("outTopic","V");
        }          
        digitalWrite(LED, HIGH);
        


    //char* charPtr = &rc;
//    snprintf (msg, 75, charPtr, value);
//    Serial.print("Publish message: ");
//    Serial.println(msg);
//    client.publish("outTopic", msg);
  }
  //client.publish("outTopic","S");
}
