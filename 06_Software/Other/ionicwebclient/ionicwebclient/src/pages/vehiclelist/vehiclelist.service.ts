import { Injectable } from '@angular/core';
import {Evehicle} from "./vehiclelist.model";

@Injectable(/*{
  providedIn: 'root',
}*/)export class VehiclelistService {
  private evehicles: Evehicle[] = [{
    id: '123',
    batterystatus: 85,
    powerstatus: true,
    title: 'myBike',
    imageUrl: 'https://static.wixstatic.com/media/7ad3d4_c765a53f0d88412bb9be02b5f71e055b~mv2_d_5302_5107_s_4_2.jpg/v1/fill/w_5000,h_4816,al_c,q_85,usm_0.66_1.00_0.01/7ad3d4_c765a53f0d88412bb9be02b5f71e055b~mv2_d_5302_5107_s_4_2.webp'
  },
    {
      id: '456',
      batterystatus: 50,
      powerstatus: false,
      title: 'myBike2',
      imageUrl: 'https://static.wixstatic.com/media/7ad3d4_c765a53f0d88412bb9be02b5f71e055b~mv2_d_5302_5107_s_4_2.jpg/v1/fill/w_5000,h_4816,al_c,q_85,usm_0.66_1.00_0.01/7ad3d4_c765a53f0d88412bb9be02b5f71e055b~mv2_d_5302_5107_s_4_2.webp'
    },

  ];

  constructor() { }

  getAllEVehicles() {
    console.log("allevehicles");
    return [...this.evehicles];
  }
  getEVehicle(evehicleId: string) {
    return {
      ...this.evehicles.find(evehicle => {
        return evehicleId === evehicle.id;
      })
    };
  }
}
