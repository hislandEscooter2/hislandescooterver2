import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {LoginPage} from '../pages/login/login';
import {VehiclelistPage} from "../pages/vehiclelist/vehiclelist";
import {VehiclelistService} from "../pages/vehiclelist/vehiclelist.service";
import {TabsPage} from "../pages/tabs/tabs";
import {ControlsPage} from "../pages/controls/controls";
import {DetailsPage} from "../pages/details/details";
import {AccountPage} from "../pages/account/account";

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    VehiclelistPage,
    TabsPage,
    ControlsPage,
    DetailsPage,
    AccountPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    VehiclelistPage,
    TabsPage,
    ControlsPage,
    DetailsPage,
    AccountPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    VehiclelistService,
  ],

})
export class AppModule {
}
