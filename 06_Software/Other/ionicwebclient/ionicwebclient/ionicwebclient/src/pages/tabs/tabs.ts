import {Component, OnInit} from '@angular/core';
import {App, IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {HomePage} from "../home/home";
import {ControlsPage} from "../controls/controls";
import {DetailsPage} from "../details/details";
import {AccountPage} from "../account/account";

/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage implements OnInit {
  homePage = HomePage;
  controlsPage = ControlsPage;
  detailsPage = DetailsPage;
  accountPage = AccountPage;

  constructor(platform: Platform, app: App) {

  }

  ngOnInit(): void {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
