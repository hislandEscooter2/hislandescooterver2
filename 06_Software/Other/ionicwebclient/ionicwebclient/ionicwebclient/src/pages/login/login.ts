import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {HomePage} from "../home/home";
import {VehiclelistPage} from "../vehiclelist/vehiclelist";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',

})
export class LoginPage {

  username: string;
  password: string;

  usernameVerified: string = 'foo';
  passwordVerified: string = 'bar';

  vehicleListPage = VehiclelistPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  login() {
    console.log("Username: " + this.username);
    console.log("Password: " + this.password);
    //if (this.username == this.usernameVerified && this.password == this.passwordVerified) {
      console.log("!!!");
      this.navCtrl.setRoot(VehiclelistPage)
    //}
  }

}
