import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MainComponent} from './main/main.component';
import {AgmCoreModule, GoogleMapsAPIWrapper} from '@agm/core';
import {Globals} from './globals';
import {MapComponent} from './map/map.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatToolbarModule, MatSidenavModule, MatIconModule, MatListModule} from '@angular/material';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import {RouterModule, Routes} from '@angular/router';
import { TestingComponent } from './testing/testing.component';
import { HttpClientModule  } from '@angular/common/http';


const appRoutes: Routes = [
  {path: 'main', component: MainComponent},
  {path: 'map', component: MapComponent},
  {path: 'test', component: TestingComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    MapComponent,
    NavigationComponent,
    TestingComponent,

  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    MatCheckboxModule,
    MatButtonModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,


    AppRoutingModule,

    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB_riTMzMDmvB7XMfT6dbDH9bGzyPgOAJ4'
      /* apiKey is required, unless you are a
      premium customer, in which case you can
      use clientId
      */
    }),
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  providers: [Globals, GoogleMapsAPIWrapper],
  bootstrap: [AppComponent]

})

export class AppModule {

}
