import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  test(){
    // Simple GET request example:
    const _this = this;
    this.http.post("http://localhost:9090/postLight", "true");
  }
}
